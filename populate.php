<?php

use App\Services\DatabaseService;
use App\Db\PDOFactory;
use App\FreelancehuntApi;
use App\Repositories\CustomerRepository;
use App\Repositories\ProjectRepository;
use App\Repositories\SkillProjectRepository;
use App\Repositories\SkillRepository;
use GuzzleHttp\Client;

$basePath = __DIR__;

require_once $basePath . '/vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable($basePath);
$dotenv->load();

$config = include($basePath . '/config.php');

//TODO: підключити DiC (PHP-DI)
$db = PDOFactory::create($config['db']);
$customerRepository = new CustomerRepository($db);
$projectRepository = new ProjectRepository($db);
$skillRepository = new SkillRepository($db);
$skillProjectRepository = new SkillProjectRepository($db);

$dbService = new DatabaseService(
    $db,
    $customerRepository,
    $projectRepository,
    $skillRepository,
    $skillProjectRepository
);

$api = new FreelancehuntApi(new Client(), $config['freelancehunt_api_token']);

foreach ($api->fetch() as $projects) {
    $dbService->saveToDb($projects);
}