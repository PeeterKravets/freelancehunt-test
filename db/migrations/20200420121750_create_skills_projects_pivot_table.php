<?php

use Phinx\Migration\AbstractMigration;

class CreateSkillsProjectsPivotTable extends AbstractMigration
{
    public function change()
    {
        $table = $this->table(
            'skills_projects',
            [
                'id' => false,
                'primary_key' => ['project_id', 'skill_id']
            ]
        );

        $table->addColumn('project_id', 'integer')
            ->addColumn('skill_id', 'integer')
            ->addForeignKey('skill_id', 'skills')
            ->addForeignKey('project_id', 'projects')
            ->create();
    }
}
