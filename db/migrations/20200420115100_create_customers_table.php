<?php

use Phinx\Migration\AbstractMigration;

class CreateCustomersTable extends AbstractMigration
{

    public function change()
    {
        $table = $this->table('customers');
        $table->addColumn('login', 'string')
            ->addColumn('first_name', 'string')
            ->addColumn('last_name', 'string')
            ->create();
    }
}
