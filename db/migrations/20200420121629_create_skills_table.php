<?php

use Phinx\Migration\AbstractMigration;

class CreateSkillsTable extends AbstractMigration
{

    public function change()
    {
        $table = $this->table('skills');
        $table->addColumn('name', 'string')
            ->create();
    }
}
