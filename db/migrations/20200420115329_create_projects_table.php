<?php

use Phinx\Migration\AbstractMigration;

class CreateProjectsTable extends AbstractMigration
{

    public function change()
    {
        $table = $this->table('projects');
        $table->addColumn('title', 'string')
            ->addColumn('url', 'string')
            ->addColumn('budget', 'float', ['null' => true])
            ->addColumn('customer_id', 'integer', ['null' => true])
            ->addColumn('published_at', 'datetime', ['null' => true])
            ->addForeignKey('customer_id', 'customers')
            ->create();
    }
}
