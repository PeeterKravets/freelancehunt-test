1. Зкопіювати .env.example -> .env
2. Вставити токен в FREELANCEHUNT_API_TOKEN
3. docker-compose up
4. docker-compose run --rm composer install
5. docker-compose run --rm php-cli vendor/bin/phinx migrate
6. docker-compose run --rm php-cli php populate.php
7. http://127.0.0.1:8010