<?php

//TODO: добавити Privatbank API
//TODO: написати тести
//TODO: позбавитись від рекурсії в FreelanceehuntApi класі.
// Для цього зробити додатковий перший запит, де дістати номер останьої сторінки,
// і далі запускати ->fetch() в циклі for
//TODO: підключити DiC (PHP-DI)
//TODO: оптимізувати репозиторії, записувати дані в БД через insertMany
//TODO: підключити PSR-7 Request / Response бібліотеку - напр. zend-diactoros

use App\Controllers\HomeController;
use App\Db\PDOFactory;
use App\Repositories\ProjectRepository;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

$basePath = __DIR__.'/..';

require_once $basePath . '/vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable($basePath);
$dotenv->load();

$config = include($basePath . '/config.php');

$loader = new FilesystemLoader($basePath . '/templates');
$template = new Environment($loader);

$db = PDOFactory::create($config['db']);
$projectRepository = new ProjectRepository($db);
$response = new HomeController($projectRepository, $template);

//TODO: підключити PSR-7 Request / Response бібліотеку - напр. zend-diactoros
echo $response($_GET);
