const pieChart = document.getElementById('pie-chart');

new Chart(pieChart.getContext('2d'), {
  type: 'pie',
  data: {
    datasets: [{
      data: JSON.parse(pieChart.dataset.sums),
      backgroundColor: [
        'rgba(75, 192, 192, 0.4)',
        'rgba(153, 102, 255, 0.4)',
        'rgba(255, 159, 64, 0.4)',
        'rgba(154, 56, 99, 0.4)',
      ],
    }],
    labels: [
      'Менше 500',
      '500-1000',
      '1000-5000',
      'Більше 5000',
    ]
  }
});