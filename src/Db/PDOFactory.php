<?php

namespace App\Db;

class PDOFactory
{
    /**
     * @param array $config
     * @return \PDO
     */
    public static function create(array $config): \PDO
    {
        $dsn = "mysql:host={$config['host']};port={$config['port']};dbname={$config['name']};charset=utf8mb4";

        return new \PDO(
            $dsn,
            $config['username'],
            $config['password'],
            $config['options']
    );
    }
}