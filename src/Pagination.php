<?php


namespace App;

use App\Repositories\PaginationInterface;
use Pagerfanta\Adapter\CallbackAdapter;
use Pagerfanta\Pagerfanta;
use Pagerfanta\View\TwitterBootstrap3View;
use Pagerfanta\View\ViewInterface;

class Pagination
{
    /**
     * @var Pagerfanta
     */
    private Pagerfanta $pagerfanta;

    /**
     * @var ViewInterface
     */
    private ViewInterface $view;

    /**
     * @var int
     */
    private int $page;

    /**
     * @var int
     */
    private int $perPage;

    /**
     * @var string
     */
    private string $url;

    /**
     * Pagination constructor.
     * @param  Pagerfanta  $pagerfanta
     * @param  ViewInterface  $view
     * @param  int  $page
     * @param  int  $perPage
     */
    public function __construct(Pagerfanta $pagerfanta, ViewInterface $view, int $page, int $perPage)
    {
        $this->pagerfanta = $pagerfanta;
        $this->view = $view;
        $this->page = $page;
        $this->perPage = $perPage;
    }

    /**
     * @param  PaginationInterface  $repository
     * @param  int  $page
     * @param  int  $perPage
     * @return static
     */
    public static function create(PaginationInterface $repository, int $page, int $perPage): self
    {
        $adapter = new CallbackAdapter(
            [$repository, 'countAll'],
            [$repository, 'fetchAll']
        );

        return new self(new Pagerfanta($adapter), new TwitterBootstrap3View(), $page, $perPage);
    }

    public function getResults(): array
    {
        $this->pagerfanta->setMaxPerPage($this->perPage);
        $this->pagerfanta->setCurrentPage($this->page);

        return $this->pagerfanta->getCurrentPageResults();
    }


    public function renderView()
    {
        $url = $this->url ?? explode('?', $_SERVER['REQUEST_URI'])[0];
        $routeGenerator = fn($page) => $url . '?page='.$page;

        return $this->view->render($this->pagerfanta, $routeGenerator);
    }

    public function setUrl(string $url)
    {
        $this->url = $url;
    }
}