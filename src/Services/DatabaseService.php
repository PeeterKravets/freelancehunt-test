<?php


namespace App\Services;

use App\Repositories\CustomerRepository;
use App\Repositories\ProjectRepository;
use App\Repositories\SkillProjectRepository;
use App\Repositories\SkillRepository;

class DatabaseService
{
    /**
     * @var CustomerRepository
     */
    private CustomerRepository $customerRepository;

    /**
     * @var ProjectRepository
     */
    private ProjectRepository $projectRepository;

    /**
     * @var SkillRepository
     */
    private SkillRepository $skillRepository;

    /**
     * @var SkillProjectRepository
     */
    private SkillProjectRepository $skillProjectRepository;

    /**
     * @var \PDO
     */
    private $db;

    /**
     * @param  CustomerRepository  $customerRepository
     * @param  ProjectRepository  $projectRepository
     * @param  SkillRepository  $skillRepository
     * @param  SkillProjectRepository  $skillProjectRepository
     */
    public function __construct(
        \PDO $db,
        CustomerRepository $customerRepository,
        ProjectRepository $projectRepository,
        SkillRepository $skillRepository,
        SkillProjectRepository $skillProjectRepository
    )
    {
        $this->db = $db;
        $this->customerRepository = $customerRepository;
        $this->projectRepository = $projectRepository;
        $this->skillRepository = $skillRepository;
        $this->skillProjectRepository = $skillProjectRepository;
    }

    public function saveToDb(array $projects)
    {
        try {
            $this->db->beginTransaction();
            array_map(function ($project) {

                $title = $project['attributes']['name'];
                $url = $project['links']['self']['web'];
                $budget = $project['attributes']['budget'] ? $project['attributes']['budget']['amount'] : null;
                $publishedAt = $project['attributes']['published_at'] ?
                    (new \DateTime($project['attributes']['published_at']))->format('Y-m-d H:i:s') :
                    null;
                $skills = $project['attributes']['skills'];
                $login = $project['attributes']['employer'] ? $project['attributes']['employer']['login'] : null;
                $firstName = $project['attributes']['employer']['first_name'];
                $lastName = $project['attributes']['employer']['last_name'];

                $customerId = null;
                if ($login) {
                    $customerId = $this->customerRepository->findId($login);
                    if (!$customerId) {
                        $customerId = $this->customerRepository->insert($login, $firstName, $lastName);
                    }
                }

                $projectId = $this->projectRepository->findId($url);
                if (!$projectId) {
                    $projectId = $this->projectRepository->insert($title, $url, $budget, $publishedAt, $customerId);
                }

                foreach ($skills as $skill) {
                    $name = $skill['name'];
                    $skillId = $this->skillRepository->findId($name);
                    if (!$skillId) {
                        $skillId = $this->skillRepository->insert($name);
                    }

                    $skillIsAttached = $this->skillProjectRepository->exists($projectId, $skillId);
                    if (!$skillIsAttached) {
                        $this->skillProjectRepository->insert($projectId, $skillId);
                    }
                }
            }, $projects);

            $this->db->commit();
        } catch (\Exception $e) {
            $this->db->rollback();
        }
    }
}