<?php

namespace App;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class FreelancehuntApi
{
    private $url = 'https://api.freelancehunt.com/v2/projects';

    /**
     * @var Client
     */
    private Client $httpClient;

    /**
     * @var string
     */
    private string $apiToken;

    /**
     * FreelancehuntApi constructor.
     * @param  Client  $httpClient
     * @param  string  $apiToken
     */
    public function __construct(Client $httpClient, string $apiToken)
    {
        $this->httpClient = $httpClient;
        $this->apiToken = $apiToken;
    }

    public function fetch($page = 1)
    {
        $url = $this->generateUrl($page);

        $response = $this->httpClient->get($url, [
            'headers' => [
                'Authorization' => 'Bearer '.$this->apiToken,
                'Content-Type' => 'application/json'
            ]
        ]);

        $body = $this->getResponseBody($response);

        //TODO: позбавитись від рекурсії. Див. TODO в public/index.php
        if (isset($body['links']['next'])) {
            $nextPage = explode('[number]=', $body['links']['next'])[1];
            yield from $this->fetch($nextPage);
        }

        yield $body['data'];
    }

    private function generateUrl($page): string
    {
        return $this->url.'?page[number]='.$page;
    }

    private function getResponseBody(ResponseInterface $response): array
    {
        return json_decode($response->getBody()->getContents(), true);
    }
}