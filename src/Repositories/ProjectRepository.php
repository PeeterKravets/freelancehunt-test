<?php


namespace App\Repositories;


class ProjectRepository implements PaginationInterface
{
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function fetchAll(int $offset, int $limit)
    {
        $stmt = $this->db->prepare("
            SELECT
                p.*, 
                CONCAT(c.first_name, ' ', c.last_name) as customer_name, 
                c.login as customer_login
            FROM projects p
            LEFT JOIN customers c ON p.customer_id = c.id
            WHERE p.id IN (
                SELECT project_id FROM skills_projects sp 
                    JOIN skills s ON sp.skill_id = s.id 
                WHERE s.name IN ('Веб-программирование', 'PHP', 'Базы данных')
            )
            ORDER BY published_at DESC LIMIT :limit OFFSET :offset
        ");
        $stmt->bindValue(':limit', $limit, \PDO::PARAM_INT);
        $stmt->bindValue(':offset', $offset, \PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function countAll(): int
    {
        $stmt = $this->db->query("
            SELECT COUNT(*)
            FROM projects p
            LEFT JOIN customers c ON p.customer_id = c.id
            WHERE p.id IN (
                SELECT project_id FROM skills_projects sp 
                    JOIN skills s ON sp.skill_id = s.id 
                WHERE s.name IN ('Веб-программирование', 'PHP', 'Базы данных')
            )
        ");

        return $stmt->fetchColumn();
    }

    public function getSumRanges()
    {
        $stmt = $this->db->prepare('
            SELECT 
               sum(case when budget < 500 then 1 else 0 end) AS less_500,
               sum(case when budget BETWEEN 500 AND 1000 then 1 else 0 end) AS between_500_1000,
               sum(case when budget BETWEEN 1000 AND 5000 then 1 else 0 end) AS between_1000_5000,
               sum(case when budget > 5000 then 1 else 0 end) AS more_5000
            FROM projects
        ');
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_NUM)[0];
    }

    public function findId(string $url)
    {
        $stmt = $this->db->prepare('SELECT id FROM projects where url = :url');
        $stmt->execute([':url' => $url]);

        return $stmt->fetchColumn();
    }

    public function insert(string $title, string $url, ?int $budget, ?string $publishedAt, ?int $customerId)
    {
        $stmt = $this->db->prepare('INSERT INTO projects (title, url, budget, published_at, customer_id) VALUES (:title, :url, :budget, :published_at, :customer_id)');
        $stmt->execute([
            ':title' => $title,
            ':url' => $url,
            ':budget' => $budget,
            ':published_at' => $publishedAt,
            ':customer_id' => $customerId
        ]);

        return $this->db->lastInsertId();
    }
}