<?php


namespace App\Repositories;


class SkillProjectRepository
{
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function exists(int $projectId, int $skillId)
    {
        $stmt = $this->db->prepare('SELECT COUNT(*) FROM skills_projects WHERE project_id = :project_id AND skill_id = :skill_id');
        $stmt->execute([
            ':project_id' => $projectId,
            ':skill_id' => $skillId
        ]);

        return $stmt->fetchColumn();
    }

    public function insert(int $projectId, int $skillId)
    {
        $stmt = $this->db->prepare('INSERT INTO skills_projects (project_id, skill_id) VALUES (:project_id, :skill_id)');
        $stmt->execute([
            'project_id' => $projectId,
            'skill_id' => $skillId
        ]);
    }
}