<?php


namespace App\Repositories;


class SkillRepository
{
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function findId(string $name)
    {
        $stmt = $this->db->prepare('SELECT id FROM skills where name = :name');
        $stmt->execute([':name' => $name]);

        return $stmt->fetchColumn();
    }

    public function insert(string $name)
    {
        $stmt = $this->db->prepare('INSERT INTO skills (name) VALUES (:name)');
        $stmt->execute([':name' => $name]);

        return $this->db->lastInsertId();
    }
}