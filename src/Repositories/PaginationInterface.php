<?php


namespace App\Repositories;


interface PaginationInterface
{
    public function countAll();

    public function fetchAll(int $offset, int $limit);
}