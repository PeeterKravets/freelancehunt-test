<?php


namespace App\Repositories;


class CustomerRepository
{
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function findId(string $login)
    {
        $stmt = $this->db->prepare('SELECT id FROM customers where login = :login');
        $stmt->execute(['login' => $login]);

        return $stmt->fetchColumn();
    }

    public function insert(string $login, string $firstName, string $lastName)
    {
        $stmt = $this->db->prepare('INSERT INTO customers (login, first_name, last_name) VALUES (:login, :first_name, :last_name)');
        $stmt->execute([
            ':login' => $login,
            ':first_name' => $firstName,
            ':last_name' => $lastName
        ]);

        return $this->db->lastInsertId();
    }


}