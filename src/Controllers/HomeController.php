<?php

namespace App\Controllers;

use App\Pagination;
use App\Repositories\ProjectRepository;
use Twig\Environment;

class HomeController
{
    private const PER_PAGE = 10;

    /**
     * @var ProjectRepository
     */
    private ProjectRepository $projectRepository;

    /**
     * @var Environment
     */
    private Environment $template;

    /**
     * HomeController constructor.
     * @param  ProjectRepository  $projectRepository
     * @param  Environment  $template
     */
    public function __construct(ProjectRepository $projectRepository, Environment $template)
    {
        $this->projectRepository = $projectRepository;
        $this->template = $template;
    }

    public function __invoke(array $request)
    {
        $paginator = Pagination::create(
            $this->projectRepository,
            $request['page'] ?? 1,
            self::PER_PAGE
        );

        $projects = $paginator->getResults();
        $paginatorView = $paginator->renderView();
        $sumRanges = $this->projectRepository->getSumRanges();

        return $this->template->render('home.html.twig', [
            'projects' => $projects,
            'paginatorView' => $paginatorView,
            'sumRanges' => $sumRanges
        ]);
    }
}